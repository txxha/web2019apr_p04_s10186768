﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using web_s10186768.Models;

namespace web_s10186768.DAL
{
    public class BranchDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor
        public BranchDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "NPBookConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);

        }

        public List<Branch> GetAllBranches()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Branch", conn);

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "BranchDetails");

            conn.Close();


            List<Branch> branchList = new List<Branch>();
            foreach(DataRow row in result.Tables["BranchDetails"].Rows)
            {
                branchList.Add(
                    new Branch
                    {
                        BranchNo = Convert.ToInt32(row["BranchNo"]),
                        Address = row["Address"].ToString(),
                        Telephone = row["TelNo"].ToString()
                    }
                   );

            }
            return branchList;

        }

        public List<Staff> GetBranchStaff(int branchNo)
        {
            //Instantiate a SqlCommand object, supply it with a SELECT SQL
            //statement which retrieves all attributes of a branch record.
            SqlCommand cmd = new SqlCommand
            ("SELECT * FROM Staff WHERE BranchNo = @selectedBranch", conn);
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the branchNo property of the Branch class.
            cmd.Parameters.AddWithValue("@selectedBranch", branchNo);
            //Instantiate a DataAdapter object, pass the SqlCommand
            //object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            // Create a DataSet object result
            DataSet result = new DataSet();
            //Open a database connection.
            conn.Open();
            //Use DataAdapter to fetch data to a table "StaffDetails" in DataSet.
            da.Fill(result, "StaffDetails");
            //Close database connection
            conn.Close();
            List<Staff> staffList = new List<Staff>();
            foreach (DataRow row in result.Tables["StaffDetails"].Rows)
            {
                staffList.Add(
                new Staff
                {
                    StaffId = Convert.ToInt32(row["StaffID"]),
                    Name = row["Name"].ToString(),
                    Gender = Convert.ToChar(row["Gender"]),
                    DOB = Convert.ToDateTime(row["DOB"]),
                    Salary = Convert.ToDecimal(row["Salary"]),
                    Nationality = row["Nationality"].ToString(),
                    Email = row["EmailAddr"].ToString(),
                    IsFullTime = Convert.ToBoolean(row["Status"]),
                    BranchNo = branchNo
                }
                );
            }
            return staffList;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using web_s10186768.Models;

namespace web_s10186768.DAL
{
    public class StaffDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor
        public StaffDAL()
        {
            //Locate the appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            //Read ConnectionString from appsettings.json file
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "NPBookConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Staff> GetAllStaff()
        {
            //Instantiate a SqlCommand object, supply it with a
            //SELECT SQL statement that operates against the database,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand(
            "SELECT * FROM Staff ORDER BY StaffID", conn);
            //Instantiate a DataAdapter object and pass the
            //SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object to contain records get from database
            DataSet result = new DataSet();
            //Open a database connection
            conn.Open();
            //Use DataAdapter, which execute the SELECT SQL through its
            //SqlCommand object to fetch data to a table "StaffDetails"
            //in DataSet "result".
            da.Fill(result, "StaffDetails");
            //Close the database connection
            conn.Close();
            //Transferring rows of data in DataSet’s table to “Staff” objects
            List<Staff> staffList = new List<Staff>();
            foreach (DataRow row in result.Tables["StaffDetails"].Rows)
            {
                int? branchNo;
                //BranchNo column not null
                if (!DBNull.Value.Equals(row["BranchNo"]))
                    branchNo = Convert.ToInt32(row["BranchNo"]);
                else
                    branchNo = null;
                staffList.Add(
                new Staff
                {
                    StaffId = Convert.ToInt32(row["StaffID"]),
                    Name = row["Name"].ToString(),
                    Gender = Convert.ToChar(row["Gender"]),
                    DOB = Convert.ToDateTime(row["DOB"]),
                    Salary = Convert.ToDecimal(row["Salary"]),
                    Nationality = row["Nationality"].ToString(),
                    Email = row["EmailAddr"].ToString(),
                    IsFullTime = Convert.ToBoolean(row["Status"]),
                    BranchNo = branchNo
                }
                );
            }
            return staffList;
        }

        public int Add(Staff staff)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement
            //which will return the auto-generated StaffID after insertion,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
            ("INSERT INTO Staff (Name, Gender, DOB, Salary, " +
            "EmailAddr, Nationality, Status) " +
            "OUTPUT INSERTED.StaffID " +
            "VALUES(@name, @gender, @dob, @salary, " +
            "@email, @country, @status)", conn);
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", staff.Name);
            cmd.Parameters.AddWithValue("@gender", staff.Gender);
            cmd.Parameters.AddWithValue("@dob", staff.DOB);
            cmd.Parameters.AddWithValue("@salary", staff.Salary);
            cmd.Parameters.AddWithValue("@email", staff.Email);
            cmd.Parameters.AddWithValue("@country", staff.Nationality);
            cmd.Parameters.AddWithValue("@status", staff.IsFullTime);
            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            staff.StaffId = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return staff.StaffId;
        }

        public bool IsEmailExist(string email)
        {
            SqlCommand cmd = new SqlCommand
            ("SELECT StaffID FROM Staff WHERE EmailAddr=@selectedEmail", conn);
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            SqlDataAdapter daEmail = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            //Use DataAdapter to fetch data to a table "EmailDetails" in DataSet.
            daEmail.Fill(result, "EmailDetails");
            conn.Close();
            if (result.Tables["EmailDetails"].Rows.Count > 0)
                return true; //The email exists for another staff
            else
                return false; // The email address given does not exist
        }

        public Staff GetDetails(int staffId)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Staff WHERE StaffID = @selectedStaffID", conn);

            cmd.Parameters.AddWithValue("@selectedStaffID", staffId);

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "StaffDetails");

            conn.Close();

            Staff staff = new Staff();
            if (result.Tables["StaffDetails"].Rows.Count > 0)
            {
                staff.StaffId = staffId;

                DataTable table = result.Tables["StaffDetails"];
                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                    staff.Name = table.Rows[0]["Name"].ToString();                if (!DBNull.Value.Equals(table.Rows[0]["Gender"]))
                    staff.Gender = Convert.ToChar(table.Rows[0]["Gender"]);
                if (!DBNull.Value.Equals(table.Rows[0]["DOB"]))
                    staff.DOB = Convert.ToDateTime(table.Rows[0]["DOB"]);
                if (!DBNull.Value.Equals(table.Rows[0]["Nationality"]))
                    staff.Nationality = table.Rows[0]["Nationality"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["EmailAddr"]))
                    staff.Email = table.Rows[0]["EmailAddr"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Salary"]))
                    staff.Salary = Convert.ToDecimal(table.Rows[0]["Salary"]);
                if (!DBNull.Value.Equals(table.Rows[0]["Status"]))
                    staff.IsFullTime = Convert.ToBoolean(table.Rows[0]["Status"]);
                if (!DBNull.Value.Equals(table.Rows[0]["BranchNo"]))
                    staff.BranchNo = Convert.ToInt32(table.Rows[0]["BranchNo"]);
                return staff; // No error occurs
            }
            else
            {
                return null; // Record not found
            }



        }


        public int Update(Staff staff)
        {
            SqlCommand cmd = new SqlCommand
            ("UPDATE Staff SET Salary=@salary, Status=@status, BranchNo=@branchNo" +
            " WHERE StaffID = @selectedStaffID", conn);            cmd.Parameters.AddWithValue("@salary", staff.Salary);            cmd.Parameters.AddWithValue("@status", staff.IsFullTime);            if (staff.BranchNo != null) // A branch is assigned
                cmd.Parameters.AddWithValue("@branchNo", staff.BranchNo.Value);
            else // No branch is assigned
                cmd.Parameters.AddWithValue("@branchNo", DBNull.Value);            cmd.Parameters.AddWithValue("@selectedStaffID", staff.StaffId);
            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();

            return count;


        }

        public int Delete(int staffId)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement
            //to delete a staff record specified by a Staff ID.
            SqlCommand cmd = new SqlCommand("DELETE FROM Staff " +
            "WHERE StaffID = @selectStaffID", conn);
            cmd.Parameters.AddWithValue("@selectStaffID", staffId);
            //Open a database connection.
            conn.Open();
            int rowCount;
            //Execute the DELETE SQL to remove the staff record.
            rowCount = cmd.ExecuteNonQuery();
            //Close database connection.
            conn.Close();
            //Return number of row of staff record deleted.
            return rowCount;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_s10186768.Models
{
    public class Staff
    {

        [Display(Name = "ID")]
        public int StaffId { get; set; }

        [StringLength(50, ErrorMessage =
        "Invalid value! Please enter a value from 1 to 10")]
        [Required]
        public string Name { get; set; }

        public char Gender { get; set; }


        [Display(Name ="Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime DOB { get; set; }

        public string Nationality { get; set; }


        [Display(Name = "Email Address")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$")]
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists(ErrorMessage = "Email address already exists!")]
        public string Email { get; set; }

        [Display(Name ="Monthly Salary(SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        [Range(1.00, 10000.00)]
        public decimal Salary { get; set; }

        [Display(Name = "Full-Time Staff")]
        public bool IsFullTime { get; set; }

        public int? BranchNo { get; set; }

    }
}

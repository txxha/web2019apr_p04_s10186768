﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using web_s10186768.DAL;
namespace web_s10186768.Models
{
    public class ValidateEmailExists : ValidationAttribute
    {
        private StaffDAL staffContext = new StaffDAL();
        public override bool IsValid(object value)
        {
            string email = Convert.ToString(value);
            if (staffContext.IsEmailExist(email))
                return false; // validation failed
            else
                return true; // validation passed
        }
    }
}


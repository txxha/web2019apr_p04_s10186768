﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_s10186768.Models
{
    public class Vote
    {
        [Display(Name = "Book ID")]
        public int BookId { get; set; }
        public string Justification { get; set; }
    }
}
